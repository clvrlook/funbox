$(document).ready(function(){
	$('.goods-item a').click(function(){
		var $item = $(this).closest('.goods-item');
		
		if(!$item.hasClass('selected')){
			$item.addClass('selected');
		}
		else{
			$item.removeClass('selected').removeClass('activated');
		}
		
		return false;
	});
	
	$(document).on('mouseleave', '.goods-item.selected', function(){
		$(this).addClass('activated');
	});
	
});